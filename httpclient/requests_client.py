import requests

from httpclient import HttpClient, AsyncHttpClient, HttpResponse, HttpError

__all__ = ['RequestsHttpClient', 'RequestsAsyncHttpClient']


def _fetch(**kwargs):
    try:
        response = requests.request(**kwargs)
    except Exception as e:
        raise HttpError(e)
    return HttpResponse(code=response.status_code,
                        body=response.content,
                        headers=response.headers)


class RequestsHttpClient(HttpClient):
    def fetch(self, url, **kwargs):
        method = kwargs.pop('method', 'GET')
        body = kwargs.pop('body', None)
        return _fetch(method=method, url=url, data=body, **kwargs)


class RequestsAsyncHttpClient(AsyncHttpClient):
    def initialize(self, executor=None, max_workers=10, *args, **kwargs):
        # async is not common, keep dependencies to a minimum
        from concurrent.futures import ThreadPoolExecutor

        if executor is None:
            self._executor = ThreadPoolExecutor(max_workers=max_workers)
        else:
            self._executor = executor

    def fetch(self, url, **kwargs):
        method = kwargs.pop('method', 'GET')
        body = kwargs.pop('body', None)
        return self._executor.submit(
            _fetch, method=method, url=url, data=body, **kwargs)
