"""
This module implements blocking and non-blocking HTTP clients using Tornado's
HTTP clients.

Tornado provides ``simple_httpclient`` and ``curl_httpclient``. Simple client
is the default.

Using CURL client::

    import tornado.httpclient
    import httpclient

    tornado.httpclient.AsyncHTTPClient.configure(
        'tornado.curl_httpclient.CurlAsyncHTTPClient')
    httpclient.HttpClient.configure(
        'httpclient.tornado_client.TornadoHttpClient')

    ...
    client = AsyncHttpClient()
    response = yield client.get('http://google.com')
    ...

"""
import tornado.httpclient
import tornado.gen

from httpclient import HttpClient, AsyncHttpClient, HttpResponse, HttpError

__all__ = ['TornadoHttpClient', 'TornadoAsyncHttpClient']


class TornadoHttpClient(HttpClient):
    def initialize(self, *args, **kwargs):
        self._client = tornado.httpclient.HTTPClient(*args, **kwargs)

    def fetch(self, url, **kwargs):
        try:
            response = self._client.fetch(url, **kwargs)
        except Exception as e:
            raise HttpError(e)
        return HttpResponse(code=response.code,
                            body=response.body,
                            headers=response.headers)


class TornadoAsyncHttpClient(AsyncHttpClient):
    def initialize(self, *args, **kwargs):
        self._client = tornado.httpclient.AsyncHTTPClient(*args, **kwargs)

    @tornado.gen.coroutine
    def fetch(self, url, **kwargs):
        try:
            response = yield self._client.fetch(url, **kwargs)
        except Exception as e:
            raise HttpError(e)
        raise tornado.gen.Return(HttpResponse(code=response.code,
                                              body=response.body,
                                              headers=response.headers))
