import logging
import tornado.ioloop
import tornado.web
from tornado import gen

from httpclient import HttpClient, AsyncHttpClient, HttpError

log = logging.getLogger('httpclient.app')


class MainHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):

        #AsyncHttpClient.configure('httpclient.tornado_client.TornadoAsyncHttpClient')
        #HttpClient.configure('httpclient.tornado_client.TornadoHttpClient')
        client = AsyncHttpClient()
        try:
            response = yield client.fetch('http://google.com')
            log.info('response is %s', response.body)
        except HttpError:
            log.error('uh oh!')

        self.write('Hello, world')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    app = tornado.web.Application([
        (r'/', MainHandler),
    ], debug=True)

    app.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
