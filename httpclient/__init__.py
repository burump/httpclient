"""
This module provides a common interface for HTTP clients and a couple reference
implementations (``requests_client`` and ``tornado_client``) for both
blocking and non-blocking clients.

All implementations can either be instantiated directly or configured using
either `HttpClient.configure` or `AsyncHttpClient.configure` methods.

Requests implementation is the default, tornado client can be enabled with::

    HttpClient.configure('httpclient.tornado_client.TornadoHttpClient')

"""

from tornado.util import Configurable

__all__ = ['HttpClient', 'AsyncHttpClient', 'HttpResponse', 'HttpError']


class HttpClientBase(Configurable):
    def fetch(self, url, method=None, headers=None, body=None, **kwargs):
        """Send an HTTP request, returning an `HttpResponse`.
        If an error occurs during the request, an `HttpError` is raised.
        """
        raise NotImplementedError()

    def get(self, url, **kwargs):
        if 'method' in kwargs:
            del kwargs['method']
        return self.fetch(url, method='GET', **kwargs)

    def post(self, url, **kwargs):
        if 'method' in kwargs:
            del kwargs['method']
        return self.fetch(url, method='POST', **kwargs)

    def put(self, url, **kwargs):
        if 'method' in kwargs:
            del kwargs['method']
        return self.fetch(url, method='PUT', **kwargs)

    def delete(self, url, **kwargs):
        if 'method' in kwargs:
            del kwargs['method']
        return self.fetch(url, method='DELETE', **kwargs)


class HttpClient(HttpClientBase):
    """A blocking HTTP client.

    Example usage::

        import httpclient

        client = httpclient.HttpClient()
        response = client.get('http://google.com')
        print 'status code: %s, body: %s' % (response.code, response.body)
    """
    @classmethod
    def configurable_base(cls):
        return HttpClient

    @classmethod
    def configurable_default(cls):
        from httpclient.requests_client import RequestsHttpClient
        return RequestsHttpClient


class AsyncHttpClient(HttpClientBase):
    """A non-blocking HTTP client.

    Example usage::

        import httpclient

        def print_result(future):
            response = future.result()
            print 'status code: %s, body: %s' % (response.code, response.body)

        client = httpclient.AsyncHttpClient()
        future = client.get('http://google.com')
        future.add_done_callback(print_result)


    Tornado usage::

        from tornado import gen
        import httpclient

        @gen.coroutine
        def get(self):
            client = AsyncHttpClient()
            response = yield client.get('http://google.com')
            print 'status code: %s, body: %s' % (response.code, response.body)

    """
    @classmethod
    def configurable_base(cls):
        return AsyncHttpClient

    @classmethod
    def configurable_default(cls):
        from httpclient.requests_client import RequestsAsyncHttpClient
        return RequestsAsyncHttpClient


class HttpResponse(object):
    def __init__(self, code, body=None, headers=None):
        self.code = code
        self.body = body
        if headers is None:
            headers = {}
        self.headers = headers


class HttpError(Exception):
    pass
